<?php

/**
 * @file
 * Provides a simple email signup form. Views interface functions.
 */

/**
 * Implements hook_views_default_views().
 */
function email_signup_views_default_views() {
  // Begin copy and paste of output from the Export tab of a view.

$view = new view();
$view->name = 'email_signup_addresses';
$view->description = 'A table listing email signups.';
$view->tag = 'email_signup';
$view->base_table = 'email_signup_addresses';
$view->human_name = 'Email Signup Addresses';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Email Signup Addresses';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer email signups';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'addrid' => 'addrid',
  'email' => 'email',
  'submitted' => 'submitted',
);
$handler->display->display_options['style_options']['default'] = 'addrid';
$handler->display->display_options['style_options']['info'] = array(
  'addrid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'email' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'submitted' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Field: Email Signup: addrid */
$handler->display->display_options['fields']['addrid']['id'] = 'addrid';
$handler->display->display_options['fields']['addrid']['table'] = 'email_signup_addresses';
$handler->display->display_options['fields']['addrid']['field'] = 'addrid';
$handler->display->display_options['fields']['addrid']['label'] = 'Address ID';
/* Field: Email Signup: email */
$handler->display->display_options['fields']['email']['id'] = 'email';
$handler->display->display_options['fields']['email']['table'] = 'email_signup_addresses';
$handler->display->display_options['fields']['email']['field'] = 'email';
$handler->display->display_options['fields']['email']['label'] = 'Email Address';
$handler->display->display_options['fields']['email']['element_label_colon'] = FALSE;
/* Field: Email Signup: submitted */
$handler->display->display_options['fields']['submitted']['id'] = 'submitted';
$handler->display->display_options['fields']['submitted']['table'] = 'email_signup_addresses';
$handler->display->display_options['fields']['submitted']['field'] = 'submitted';
$handler->display->display_options['fields']['submitted']['label'] = 'Date Submitted';
$handler->display->display_options['fields']['submitted']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['submitted']['date_format'] = 'long';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'admin/people/email_signups';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Email Signups';
$handler->display->display_options['menu']['description'] = 'Email addresses submitted to this site';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;

    // (Export ends here.)
  
  // Add view to list of views to provide.
  $views[$view->name] = $view;

  // ...Repeat all of the above for each view the module should provide.

  // At the end, return array of default views.
  return $views;  
}
