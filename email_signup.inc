<?php

/**
 * @file
 * Provides a simple email signup form. Main functions.
 */

/**
 * Generate the email signup block
 */
function email_signup_signup_block() {
  $block = array();
  $block['title'] = '';
  $block['content'] = drupal_get_form("email_signup_entry_form");
  return $block;
}

/**
 * Define a simple email address entry form
 */
function email_signup_entry_form($form, &$form_state) {
  $form = array();
  $form['entry_field'] = array(
    '#type' => 'textfield',
    '#title' => variable_get('email_signup_form_label', EMAIL_SIGNUP_DEFAULT_LABEL),
    '#placeholder' => t('Your email address'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Updates'),
  );
  return $form;
}

/**
 * Submit handler.  If it's a well-formed address, save it;
 * otherwise discard it.
 *
 * Check the address, present a message, save it.
 */
function email_signup_entry_form_submit($form, &$form_state) {
  $address = $form_state['values']['entry_field'];
  if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
    email_signup_save_data($address);
  }
}

/**
 * Define an admin settings form.
 */
function email_signup_admin_form($form, &$form_state) {
  $form = array();
  $form['email_signup_form_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Entry box label'),
    '#description' => t('Text for the label next to the entry box.'),
    '#size' => 60,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#default_value' => variable_get('email_signup_form_label', EMAIL_SIGNUP_DEFAULT_LABEL),
  );
  $form['email_signup_form_message_new'] = array(
    '#type' => 'textfield',
    '#title' => t('Completion Message: New'),
    '#description' => t('Message presented upon successful signup.'),
    '#size' => 60,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#default_value' => variable_get('email_signup_form_message_new', EMAIL_SIGNUP_DEFAULT_NEW_MESSAGE),
  );
  $form['email_signup_form_message_existing'] = array(
    '#type' => 'textfield',
    '#title' => t('Completion Message: Existing'),
    '#description' => t('Message presented if email address is already on the list.'),
    '#size' => 60,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#default_value' => variable_get('email_signup_form_message_existing', EMAIL_SIGNUP_DEFAULT_EXISTING_MESSAGE),
  );
  return system_settings_form($form);
}

/**
 * Save the email address the user gave us.
 */
function email_signup_save_data($address) {
  $values = new stdClass;
  $values->email = $address;
  $values->submitted = time();
  if ($rec = email_signup_load_data($address)) {
    // existing record -- no need to write anything!
    //$values->addrid = $rec->addrid;
    //$values->submitted = $rec->submitted;
    //drupal_write_record('email_signup_addresses', $values, 'addrid');
    $msg = variable_get('email_signup_form_message_existing', EMAIL_SIGNUP_DEFAULT_EXISTING_MESSAGE);
    drupal_set_message($msg, "status");
  }
  else {
    // new record
    drupal_write_record('email_signup_addresses', $values);
    $msg = variable_get('email_signup_form_message_new', EMAIL_SIGNUP_DEFAULT_NEW_MESSAGE);
    drupal_set_message($msg, "status");
  }  
}

/**
 * Retrieve an address record from the database
 */
function email_signup_load_data($address) {
  $qry = 'SELECT * FROM {email_signup_addresses} WHERE email = :address';
  $result = db_query($qry, array(':address' => $address));
  $record = FALSE;
  foreach ($result as $record) {
    return $record;
  }
  return $record;
}

/**
 * Delete an address from the database
 */
function email_signup_delete_address($address) {
  $qry = 'DELETE FROM {email_signup_addresses} WHERE email = :address';
  db_query($qry, array(':address' => $address));
}
