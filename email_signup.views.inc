<?php

/**
 * @file
 * Provides a simple email signup form. Views interface functions.
 */

/**
 * Implements hook_views_data().
 */
function email_signup_views_data() {
  $data = array();  
  $data['email_signup_addresses']['table']['group']  = t('Email Signup');
  $data['email_signup_addresses']['table']['base'] = array(      
    'field' => 'logid',
    'title' => t('Database table: email_signup_addresses'),
    'help' => t('Email addresses submitted by users'),
    'weight' => 10,
    'database' => 'default',
  );
  $data['email_signup_addresses']['addrid'] = array(
    'title' => t('addrid'),
    'help' => t('A unique email address identifier'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['email_signup_addresses']['email'] = array(
    'title' => t('email'),
    'help' => t('Email address'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['email_signup_addresses']['submitted'] = array(
    'title' => t('submitted'),
    'help' => t('When this address was first submitted'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
